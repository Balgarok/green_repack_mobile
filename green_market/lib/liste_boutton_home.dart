import 'package:flutter/material.dart';

class ListeBouttonHome extends StatefulWidget {
  const ListeBouttonHome({Key? key}) : super(key: key);

  @override
  _ListeBouttonHomeState createState() => _ListeBouttonHomeState();
}

class _ListeBouttonHomeState extends State<ListeBouttonHome> {
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate(
        [ Column(
          children: [TextButton(
                child: const Text(
                  'Inscription Association',
                  style: TextStyle(fontSize: 20.0),
                ),
                onPressed: () =>
                    {Navigator.of(context).pushNamed('/inscription')},
              ),
             TextButton(
                child: const Text(
                  'Connexion Client/Marchand',
                  style: TextStyle(fontSize: 20.0),
                ),
                onPressed: () =>
                    {Navigator.of(context).pushNamed('/connexionC')},
              ),
             TextButton(
                child: const Text(
                  'Connexion Association',
                  style: TextStyle(fontSize: 20.0),
                ),
                onPressed: () =>
                    {Navigator.of(context).pushNamed('/connexionA')},
              ), TextButton(
                child: const Text(
                  'Connexion Employé',
                  style: TextStyle(fontSize: 20.0),
                ),
                onPressed: () =>
                    {Navigator.of(context).pushNamed('/connexionE')},
              ),
            
          ],
        ),
      
        ]
      )
    );
  }
}
