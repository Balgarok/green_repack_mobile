import 'package:flutter/material.dart';

class AccueilAssociation extends StatefulWidget {
  const AccueilAssociation({Key? key}) : super(key: key);

  @override
  _AccueilAssociationState createState() => _AccueilAssociationState();
}

class _AccueilAssociationState extends State<AccueilAssociation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TextButton(onPressed: () => {
            Navigator.of(context).pushNamed('/creationProjet')
          }, child: const Text("Créer un projet")),
          TextButton(onPressed: () => {
            Navigator.of(context).pushNamed('/listeProjetA')
          }, child: Text("Mes projets"))
        ],
      )
    );
  }
}