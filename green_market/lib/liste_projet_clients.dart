import 'package:flutter/material.dart';
import 'package:green_market/models/data_transfer_client.dart';
import 'package:green_market/models/data_transfer_client_projet.dart';
import 'package:green_market/models/projet_model.dart';
import 'apiCall/call_api.dart';

class ListeProjetClients extends StatefulWidget {
  const ListeProjetClients({Key? key}) : super(key: key);

  @override
  _ListeProjetClientsState createState() => _ListeProjetClientsState();
}

class _ListeProjetClientsState extends State<ListeProjetClients> {
  late Future<List<ProjetModel>> listeProjets;
  late DataTransferClient transfer;

  @override
  void initState() {
    super.initState();
    transfer = ModalRoute.of(context)!.settings.arguments as DataTransferClient;
    String idAsso = transfer.idAsso;
    listeProjets = CallApi.listeProjet(idAsso);
  }

  @override
  Widget build(BuildContext context) {
    DataTransferClientProjet transferP;
    return Scaffold(
      body: FutureBuilder<List<ProjetModel>>(
        future: listeProjets,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
                itemBuilder: (context, index) {
                  var projet = (snapshot.data as List<ProjetModel>)[index];
                  return Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          projet.nom,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(projet.description),
                        TextButton(
                            onPressed: () => {
                              transferP = DataTransferClientProjet(session: transfer.session, projet: projet),
                                  Navigator.of(context).pushNamed('/vueProjetC',
                                      arguments: transferP)
                                },
                            child: const Text("Donner pour le projet"))
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
                itemCount: (snapshot.data as List<ProjetModel>).length);
          } else if (snapshot.hasError) {
            return Center(
              child: Text("${snapshot.error}"),
            );
          }
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.greenAccent,
            ),
          );
        },
      ),
    );
  }
}
