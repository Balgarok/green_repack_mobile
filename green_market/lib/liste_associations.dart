import 'package:flutter/material.dart';
import 'package:green_market/models/data_transfer_client.dart';
import 'package:green_market/models/session_client.dart';
import 'apiCall/call_api.dart';
import 'models/association_model.dart';

class ListeAssociations extends StatefulWidget {
  const ListeAssociations({Key? key}) : super(key: key);

  @override
  _ListeAssociationsState createState() => _ListeAssociationsState();
}

class _ListeAssociationsState extends State<ListeAssociations> {
  late Future<List<AssociationModel>> listeAssociations;

  @override
  void initState() {
    super.initState();
    listeAssociations = CallApi.listeAsso();
  }

  @override
  Widget build(BuildContext context) {
    DataTransferClient transfer;
    SessionClient session =
        ModalRoute.of(context)!.settings.arguments as SessionClient;
    return Scaffold(
      body: FutureBuilder<List<AssociationModel>>(
        future: listeAssociations,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
                itemBuilder: (context, index) {
                  var asso = (snapshot.data as List<AssociationModel>)[index];
                  return Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          asso.nom,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(asso.adresse),
                        const SizedBox(
                          height: 5,
                        ),
                        TextButton(
                            onPressed: () => {
                              transfer = DataTransferClient(session: session, idAsso: asso.id),
                                  Navigator.of(context).pushNamed(
                                      '/listeProjetC',
                                      arguments: transfer)
                                },
                            child: const Text("Les projets"))
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
                itemCount: (snapshot.data as List<AssociationModel>).length);
          } else if (snapshot.hasError) {
            return Center(
              child: Text("${snapshot.error}"),
            );
          }
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.greenAccent,
            ),
          );
        },
      ),
    );
  }
}
