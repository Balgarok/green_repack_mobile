import 'package:flutter/material.dart';
import 'package:green_market/apiCall/call_api.dart';
import 'package:green_market/models/data_transfer_client_projet.dart';

import 'models/projet_model.dart';

class VueProjetClient extends StatefulWidget {
  const VueProjetClient({Key? key}) : super(key: key);

  @override
  _VueProjetClientState createState() => _VueProjetClientState();
}

class _VueProjetClientState extends State<VueProjetClient> {
  late ProjetModel projet;
  late DataTransferClientProjet transferP;
  late String coins;
  TextEditingController coinController = TextEditingController();

  @override
  void initState() {
    transferP =
        ModalRoute.of(context)!.settings.arguments as DataTransferClientProjet;
    projet = transferP.projet;
    coins = CallApi.getCoin(transferP.session.userId) as String;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: Text(projet.nom)),
          Container(
              padding: const EdgeInsets.all(10), child: Text(projet.description)),
          Container(
              padding: const EdgeInsets.all(10),
              child: Text("Vous avez " + coins + " GreenCoins a disposition")),
          Container(
            padding: const EdgeInsets.all(10),
            child: TextField(
              controller: coinController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Les GreenCoins que vous voulez donner',
              ),
            ),
          ),
          Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: RaisedButton(
                textColor: Colors.white,
                color: Colors.blue,
                child: const Text('Envoi de GreenCoin'),
                onPressed: () {
                  if (coinController.toString() != "") {
                    CallApi.envoiCoin(coinController.toString(), projet.id);
                    Navigator.of(context)
                        .pushNamed('/listeAssoC', arguments: transferP.session);
                  }
                },
              ))
        ],
      ),
    );
  }
}
