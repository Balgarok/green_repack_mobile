import 'package:flutter/material.dart';
import 'package:green_market/apiCall/call_api.dart';
import 'package:green_market/models/session_association.dart';

class ConnexionAssociation extends StatefulWidget {
  const ConnexionAssociation({Key? key}) : super(key: key);

  @override
  _ConnexionAssociationState createState() => _ConnexionAssociationState();
}

class _ConnexionAssociationState extends State<ConnexionAssociation> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        const ConnexionAForm(),
        TextButton(
            onPressed: () => {
                  //test login if ok route
                  Navigator.of(context).pushNamed('/accueilA')
                },
            child: const Text("Connexion"))
      ],
    ));
  }
}

class ConnexionAForm extends StatefulWidget {
  const ConnexionAForm({Key? key}) : super(key: key);

  @override
  ConnexionAFormState createState() {
    return ConnexionAFormState();
  }
}

// Create a corresponding State class, which holds data related to the form.
class ConnexionAFormState extends State<ConnexionAForm> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'Connexion',
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'User Name',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: RaisedButton(
                        textColor: Colors.white,
                        color: Colors.blue,
                        child: const Text('Connexion'),
                        onPressed: () {
                          if (nameController.toString() != "" &&
                              passwordController.toString() != "") {
                            Future<SessionAssociation> sessionRecup =
                                CallApi.loginAsso(nameController.toString(),
                                    passwordController.toString());
                            if (sessionRecup != null) {
                              SessionAssociation session =
                                  sessionRecup as SessionAssociation;
                              Navigator.of(context).pushNamed("/listeProjetA",
                                  arguments: session);
                            }
                          }
                        }))
              ],
            )));
  }
}
