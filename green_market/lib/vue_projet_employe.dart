import 'package:flutter/material.dart';
import 'package:green_market/apiCall/call_api.dart';
import 'package:green_market/models/data_transfer_employe.dart';
import 'package:green_market/models/data_transfer_employe_projet.dart';

import 'models/projet_model.dart';

class VueProjetEmploye extends StatefulWidget {
  const VueProjetEmploye({Key? key}) : super(key: key);

  @override
  _VueProjetEmployeState createState() => _VueProjetEmployeState();
}

class _VueProjetEmployeState extends State<VueProjetEmploye> {
  late ProjetModel projet;
  late DataTransfertEmployeProjet transfer;

  @override
  void initState() {
    super.initState();
    transfer = ModalRoute.of(context)!
        .settings
        .arguments as DataTransfertEmployeProjet;
    projet = transfer.projet;
  }

  @override
  Widget build(BuildContext context) {
    bool envoi = false;
    return Container(
      child: Column(
        children: [
          Text(projet.nom),
          Text(projet.description),
          Text(projet.fin),
          TextButton(
              onPressed: () => {
                    envoi = CallApi.accepteProjet(projet.id) as bool,
                    if (envoi)
                      {
                        Navigator.of(context).pushNamed('/listeProjetE',
                            arguments: DataTransfertEmploye(
                                session: transfer.session, idAsso: projet.idAsso))
                      }
                  },
              child: const Text("Accepter le projet"))
        ],
      ),
    );
  }
}
