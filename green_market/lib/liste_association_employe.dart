import 'package:flutter/material.dart';
import 'package:green_market/models/data_transfer_employe.dart';
import 'package:green_market/models/session_employe.dart';
import 'apiCall/call_api.dart';
import 'models/association_model.dart';

class ListeAssociationEmploye extends StatefulWidget {
  const ListeAssociationEmploye({Key? key}) : super(key: key);

  @override
  _ListeAssociationEmployeState createState() =>
      _ListeAssociationEmployeState();
}

class _ListeAssociationEmployeState extends State<ListeAssociationEmploye> {
  late Future<List<AssociationModel>> listeAssociations;

  @override
  void initState() {
    super.initState();
    listeAssociations = CallApi.listeAsso();
  }

  @override
  Widget build(BuildContext context) {
    SessionEmploye session =
        ModalRoute.of(context)!.settings.arguments as SessionEmploye;
    DataTransfertEmploye transfert;
    return Scaffold(
      body: FutureBuilder<List<AssociationModel>>(
        future: listeAssociations,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
                itemBuilder: (context, index) {
                  var asso = (snapshot.data as List<AssociationModel>)[index];
                  return Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          asso.nom,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(asso.adresse),
                        const SizedBox(
                          height: 5,
                        ),
                        TextButton(
                            onPressed: () => {
                              transfert = DataTransfertEmploye(session: session, idAsso: asso.id),
                              Navigator.of(context).pushNamed(
                                      '/listeProjetE',
                                      arguments: transfert)
                                },
                            child: const Text("Les projets a accepter"))
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
                itemCount: (snapshot.data as List<AssociationModel>).length);
          } else if (snapshot.hasError) {
            return Center(
              child: Text("${snapshot.error}"),
            );
          }
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.greenAccent,
            ),
          );
        },
      ),
    );
  }
}
