import 'package:flutter/material.dart';
import 'package:green_market/models/association_model.dart';
import 'package:green_market/models/projet_model.dart';
import 'package:green_market/models/session_association.dart';
import 'package:green_market/models/session_client.dart';
import 'package:green_market/models/session_employe.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class CallApi extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  static Future<SessionClient> loginClient(String login, String password) async {
    final http.Response response = await http.post(
      "https://green-repack-backend.herokuapp.com/api/utilisateurs/login",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body:
          jsonEncode(<String, String>{'username': login, 'password': password}),
    );
    if (response.statusCode == 200) {
      var logsClient = json.decode(response.body) as SessionClient;
      return logsClient;
    } else {
      throw Exception("Impossible de se connecter");
    }
  }

  static Future<SessionEmploye> loginEmplo(String login, String password) async {
    final http.Response response = await http.post(
      "https://green-repack-backend.herokuapp.com/api/utilisateurs/loginEm",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body:
          jsonEncode(<String, String>{'username': login, 'password': password}),
    );
    if (response.statusCode == 200) {
      var logsEmplo = json.decode(response.body) as SessionEmploye;
      return logsEmplo;
    } else {
      throw Exception("Impossible de se connecter");
    }
  }

  static Future<SessionAssociation> loginAsso(String login, String password) async {
    final http.Response response = await http.post(
      "https://green-repack-backend.herokuapp.com/api/utilisateurs/loginAsso",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body:
          jsonEncode(<String, String>{'username': login, 'password': password}),
    );
    if (response.statusCode == 200) {
      var logsAsso = json.decode(response.body) as SessionAssociation;
      return logsAsso;
    } else {
      throw Exception("Impossible de se connecter");
    }
  }

  static Future<String> inscriptionAsso(String nom, String rna, String addresse,
      String password, String email) async {
    final http.Response response = await http.post(
      "https://green-repack-backend.herokuapp.com/api/utilisateurs/signupAsso",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': nom,
        'password': password,
        'email': email,
        'address': addresse,
        'rna': rna
      }),
    );
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception("Impossible de se connecter");
    }
  }

  static Future<List<AssociationModel>> listeAsso() async {
    final http.Response response =
        await http.get("https://green-repack-backend.herokuapp.com/api/associations/listAsso");
    if (response.statusCode == 200) {
      var getListeAsso = json.decode(response.body) as List;
      var listeAsso =
          getListeAsso.map((e) => AssociationModel.fromJSON(e)).toList();
      return listeAsso;
    } else {
      throw Exception("Erreur recuperation des informations");
    }
  }

  static Future<List<ProjetModel>> listeProjet(String idAsso) async {
    final http.Response response =
        await http.get("https://green-repack-backend.herokuapp.com/api/associations/listProjet/"+idAsso);
    if (response.statusCode == 200) {
      var getListeProjet = json.decode(response.body) as List;
      var listeProjet =
          getListeProjet.map((e) => ProjetModel.fromJSON(e)).toList();
      return listeProjet;
    } else {
      throw Exception("Erreur recuperation des informations");
    }
  }

  static Future<ProjetModel> getProjet(int idProjet) async {
    final http.Response response =
        await http.get("https://green-repack-backend.herokuapp.com/api/associations/projet/" + idProjet.toString());
    if (response.statusCode == 200) {
      return ProjetModel.fromJSON(json.decode(response.body));
    } else {
      throw Exception("Erreur recuperation des informations");
    }
  }

  static Future<String> getCoin(String idClient) async {
    final http.Response response =
        await http.get("https://green-repack-backend.herokuapp.com/api/utilisateurs/coins/" + idClient.toString());
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception("Erreur recuperation des informations");
    }
  }

  static Future<bool> envoiCoin(String coin, String idProjet) async {
    final http.Response response = await http.put(
      "https://green-repack-backend.herokuapp.com/api/associations/coin/" + idProjet,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{}),
    );
    if (response.statusCode == 201) {
      return true;
    } else {
      throw Exception("Erreur recuperation des informations");
    }
  }

  static Future<bool> createProjet(
      String nom, String description, String date, String idAsso) async {
    final http.Response response = await http.post(
      "https://green-repack-backend.herokuapp.com/api/utilisateurs/signupAsso",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': nom,
        'description': description,
        'association': idAsso,
        'fin': date
      }),
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Impossible de se connecter");
    }
  }

  static Future<bool> accepteProjet(String idProjet) async {
    final http.Response response = await http.put(
      "https://green-repack-backend.herokuapp.com/api/associations/accepteProjet/" + idProjet,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{}),
    );
    if (response.statusCode == 201) {
      return true;
    } else {
      throw Exception("Erreur recuperation des informations");
    }
  }
}
