import 'package:green_market/models/projet_model.dart';
import 'package:green_market/models/session_employe.dart';

class DataTransfertEmployeProjet {
  SessionEmploye session;
  ProjetModel projet;

  DataTransfertEmployeProjet({required this.session, required this.projet});
}