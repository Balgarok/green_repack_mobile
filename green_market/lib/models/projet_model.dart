import 'package:flutter/material.dart';

class ProjetModel {
  final String id;
  final String nom;
  final String description;
  final String etat;
  final String fin;
  final String idAsso;
  final String greenCoin;
  const ProjetModel(
      {required this.id,
      required this.nom,
      required this.description,
      required this.etat,
      required this.fin,
      required this.idAsso,
      required this.greenCoin});

  factory ProjetModel.fromJSON(Map<String, dynamic> parseJson) {
    return ProjetModel(
        id: parseJson['id'],
        nom: parseJson['name'],
        description: parseJson['description'],
        etat: parseJson['accepte'],
        fin: parseJson['fin'],
        idAsso: parseJson['association'],
        greenCoin: parseJson['greencoin']);
  }
}
