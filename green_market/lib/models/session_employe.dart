class SessionEmploye {
  String userId;
  String token;
  String type;

  SessionEmploye(
      {required this.userId, required this.token, required this.type});

  factory SessionEmploye.fromJSON(Map<String, dynamic> parsedJson) {
    return SessionEmploye(
        userId: parsedJson['userId'],
        token: parsedJson['token'],
        type: parsedJson['type']);
  }
}
