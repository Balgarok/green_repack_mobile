import 'package:green_market/models/projet_model.dart';
import 'package:green_market/models/session_client.dart';

class DataTransferClientProjet {
  SessionClient session;
  ProjetModel projet;

  DataTransferClientProjet({required this.session, required this.projet});
}
