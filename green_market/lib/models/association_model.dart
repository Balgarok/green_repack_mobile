class AssociationModel {
  String id;
  String nom;
  String adresse;
  String email;
  AssociationModel(
      {required this.id, required this.nom, required this.adresse, required this.email});

  factory AssociationModel.fromJSON(Map<String, dynamic> parsedJson) {
    return AssociationModel(
      id: parsedJson['id'],
      nom: parsedJson['nom'],
      adresse: parsedJson['adresse'],
      email: parsedJson['email']
    );
  }
}
