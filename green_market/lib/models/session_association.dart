class SessionAssociation {
  String userId;
  String token;
  String type;

  SessionAssociation({required this.userId, required this.token, required this.type});

  factory SessionAssociation.fromJSON(Map<String, dynamic> parsedJson) {
    return SessionAssociation(
        userId: parsedJson['userId'],
        token: parsedJson['token'],
        type: parsedJson['type']);
  }
}
