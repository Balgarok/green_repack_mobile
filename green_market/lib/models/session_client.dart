class SessionClient {
  String userId;
  String token;
  String type;

  SessionClient({required this.userId, required this.token, required this.type});

  factory SessionClient.fromJSON(Map<String, dynamic> parsedJson) {
    return SessionClient(
        userId: parsedJson['userId'], token: parsedJson['token'], type: parsedJson['type']);
  }
}
