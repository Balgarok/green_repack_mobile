import 'package:flutter/material.dart';
import 'package:green_market/models/data_transfer_employe.dart';
import 'package:green_market/models/projet_model.dart';
import 'package:green_market/models/session_employe.dart';
import 'apiCall/call_api.dart';
import 'models/data_transfer_employe_projet.dart';

class ListeProjetEmployes extends StatefulWidget {
  const ListeProjetEmployes({Key? key}) : super(key: key);

  @override
  _ListeProjetEmployesState createState() => _ListeProjetEmployesState();
}

class _ListeProjetEmployesState extends State<ListeProjetEmployes> {
  late Future<List<ProjetModel>> listeProjets;
  late SessionEmploye session;

  @override
  void initState() {
    super.initState();
    DataTransfertEmploye transfer =
        ModalRoute.of(context)!.settings.arguments as DataTransfertEmploye;
    String idAssociation = transfer.idAsso;
    session = transfer.session;
    listeProjets = CallApi.listeProjet(idAssociation);
  }

  @override
  Widget build(BuildContext context) {
    DataTransfertEmployeProjet transferProjet;
    return Scaffold(
      body: FutureBuilder<List<ProjetModel>>(
        future: listeProjets,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
                itemBuilder: (context, index) {
                  var projet = (snapshot.data as List<ProjetModel>)[index];
                  return Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          projet.nom,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(projet.description),
                        TextButton(
                            onPressed: () => {
                                  transferProjet = DataTransfertEmployeProjet(
                                      session: session, projet: projet),
                                  Navigator.of(context).pushNamed('/vueProjetE',
                                      arguments: transferProjet)
                                },
                            child: const Text("Accepter le projet"))
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
                itemCount: (snapshot.data as List<ProjetModel>).length);
          } else if (snapshot.hasError) {
            return Center(
              child: Text("${snapshot.error}"),
            );
          }
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.greenAccent,
            ),
          );
        },
      ),
    );
  }
}
