import 'package:flutter/material.dart';
import 'package:green_market/apiCall/call_api.dart';

import 'models/session_association.dart';

class CreationProjet extends StatefulWidget {
  const CreationProjet({Key? key}) : super(key: key);

  @override
  _CreationProjetState createState() => _CreationProjetState();
}

class _CreationProjetState extends State<CreationProjet> {
  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SessionAssociation session =
        ModalRoute.of(context)!.settings.arguments as SessionAssociation;
    return Scaffold(
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'Creation d\'un projet',
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nom du projet',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: descriptionController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Description',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: dateController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Date de Fin',
                    ),
                  ),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      child: const Text('Creation du projet'),
                      onPressed: () {
                        if (nameController.toString() != "" &&
                            descriptionController.toString() != "" &&
                            dateController.toString() != "") {
                          Future<bool> creation = CallApi.createProjet(
                              nameController.toString(),
                              descriptionController.toString(),
                              dateController.toString(),
                              session.userId);
                          if(creation == true){
                            Navigator.of(context).pushNamed("/listeProjetA", arguments: session);
                          }
                        }
                      },
                    ))
              ],
            )));
  }
}
