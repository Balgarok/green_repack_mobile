import 'package:flutter/material.dart';
import 'package:green_market/apiCall/call_api.dart';
import 'package:green_market/models/session_client.dart';

class ConnexionClient extends StatefulWidget {
  const ConnexionClient({Key? key}) : super(key: key);

  @override
  _ConnexionClientState createState() => _ConnexionClientState();
}

class _ConnexionClientState extends State<ConnexionClient> {
  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        const ConnexionCForm(),
        TextButton(
            onPressed: () => {
                  //test login if ok route
                  Navigator.of(context).pushNamed('/listeAssoC')
                },
            child: const Text("Connexion"))
      ],
    );
  }
}

class ConnexionCForm extends StatefulWidget {
  const ConnexionCForm({Key? key}) : super(key: key);

  @override
  ConnexionCFormState createState() {
    return ConnexionCFormState();
  }
}


class ConnexionCFormState extends State<ConnexionCForm> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
            padding: const EdgeInsets.all(10),
            child:  ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'Connexion',
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'User Name',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      child: const Text('Connexion'),
                      onPressed: () {
                        if (nameController.toString() != "" &&
                            passwordController.toString() != "") {
                          Future<SessionClient> sessionRecup =
                              CallApi.loginClient(nameController.toString(),
                                  passwordController.toString());
                          if (sessionRecup != null) {
                            SessionClient session =
                                sessionRecup as SessionClient;
                            Navigator.of(context)
                                .pushNamed("/listeAssoC", arguments: session);
                          }
                        }
                      },
                    ))
              ],
            ));
  }
}
