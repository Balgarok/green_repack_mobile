import 'package:flutter/material.dart';
import 'package:green_market/accueil_association.dart';
import 'package:green_market/connexion_association.dart';
import 'package:green_market/connexion_client.dart';
import 'package:green_market/connexion_employe.dart';
import 'package:green_market/creation_projet.dart';
import 'package:green_market/inscription.dart';
import 'package:green_market/liste_association_employe.dart';
import 'package:green_market/liste_associations.dart';
import 'package:green_market/liste_boutton_home.dart';
import 'package:green_market/liste_projet_association.dart';
import 'package:green_market/liste_projet_clients.dart';
import 'package:green_market/liste_projet_employes.dart';
import 'package:green_market/vue_projet_client.dart';
import 'package:green_market/vue_projet_employe.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Green Market',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.green,
        primarySwatch: Colors.blue,
        cardColor: Colors.white,
        appBarTheme: const AppBarTheme(
          color: Colors.teal,
          centerTitle: true,
        ),
      ),
      home: const MyHomePage(),
      routes:{
        "/inscription": (_) => const Inscription(),
        "/connexionC": (_) => const ConnexionClient(),
        "/connexionE": (_) => const ConnexionEmploye(),
        "/connexionA": (_) => const ConnexionAssociation(),
        "/accueilA": (_) => const AccueilAssociation(),
        "/listeAssoC": (_) => const ListeAssociations(),
        "/listeAssoE": (_) => const ListeAssociationEmploye(),
        "/listeProjetC": (_) => const ListeProjetClients(),
        "/listeProjetA": (_) => const ListeProjetAssociation(),
        "/listeProjetE": (_) => const ListeProjetEmployes(),
        "/creationProjet": (_) => const CreationProjet(),
        "/vueProjetC": (_) => const VueProjetClient(),
        "/vueProjetE": (_) => const VueProjetEmploye(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  

  @override
  Widget build(BuildContext context) {
    return  const Scaffold(
        body: 
          CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              pinned: true,
              title: Text("Green Repack"),
            ),
            ListeBouttonHome(),
          ]
        ), 
      )
      
    ;
  }
}
