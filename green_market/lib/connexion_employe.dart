import 'package:flutter/material.dart';
import 'package:green_market/apiCall/call_api.dart';
import 'package:green_market/models/session_employe.dart';

class ConnexionEmploye extends StatefulWidget {
  const ConnexionEmploye({Key? key}) : super(key: key);

  @override
  _ConnexionEmployeState createState() => _ConnexionEmployeState();
}

class _ConnexionEmployeState extends State<ConnexionEmploye> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        ConnexionEForm(),
        TextButton(
            onPressed: () => {
                  //test login if ok route
                  Navigator.of(context).pushNamed('/listeAssoE')
                },
            child: const Text("Connexion"))
      ],
    ));
  }
}

class ConnexionEForm extends StatefulWidget {
  const ConnexionEForm({Key? key}) : super(key: key);

  @override
  ConnexionEFormState createState() {
    return ConnexionEFormState();
  }
}

// Create a corresponding State class, which holds data related to the form.
class ConnexionEFormState extends State<ConnexionEForm> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Scaffold(
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'Connexion',
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'User Name',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      child: Text('Connexion'),
                      onPressed: () {
                        if (nameController.toString() != "" &&
                            passwordController.toString() != "") {
                          Future<SessionEmploye> sessionRecup =
                              CallApi.loginEmplo(nameController.toString(),
                                  passwordController.toString());
                          SessionEmploye session =
                              sessionRecup as SessionEmploye;
                          if (session != null) {
                            Navigator.of(context)
                                .pushNamed("/listeAssoE", arguments: session);
                          }
                        }
                      },
                    ))
              ],
            )));
  }
}
