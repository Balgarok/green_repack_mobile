import 'package:flutter/material.dart';
import 'package:green_market/models/projet_model.dart';
import 'package:green_market/models/session_association.dart';
import 'apiCall/call_api.dart';

class ListeProjetAssociation extends StatefulWidget {
  const ListeProjetAssociation({Key? key}) : super(key: key);

  @override
  _ListeProjetAssociationState createState() => _ListeProjetAssociationState();
}

class _ListeProjetAssociationState extends State<ListeProjetAssociation> {
  late Future<List<ProjetModel>> listeProjets;
  late SessionAssociation session;
  @override
  void initState() {
    super.initState();
    session =
        ModalRoute.of(context)!.settings.arguments as SessionAssociation;
    listeProjets = CallApi.listeProjet(session.userId);
  }

  @override
  Widget build(BuildContext context) {
    
    return Column(
      children: [
        ElevatedButton(
            onPressed: () {
              Navigator.of(context)
                  .pushNamed("/creationProjet", arguments: session);
            },
            child: const Text("Crée un projet")),
        Container(
          child: Scaffold(
            body: FutureBuilder<List<ProjetModel>>(
              future: listeProjets,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.separated(
                      itemBuilder: (context, index) {
                        var projet =
                            (snapshot.data as List<ProjetModel>)[index];
                        return Container(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                projet.nom,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 22),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                projet.description
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return const Divider();
                      },
                      itemCount: (snapshot.data as List<ProjetModel>).length);
                } else if (snapshot.hasError) {
                  return Center(
                    child: Text("${snapshot.error}"),
                  );
                }
                return const Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.greenAccent,
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
